package common

import "regexp"

func GetEmailsFromString(text string) []string {
	r, _ := regexp.Compile("[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-.]+")
	return r.FindAllString(text, -1)
}
