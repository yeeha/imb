package common

import "testing"
import "github.com/stretchr/testify/assert"

func TestGetEmailsFromString(t *testing.T) {
	var emails []string
	emails = GetEmailsFromString("testong")
	assert.Equal(t, 0, len(emails), "no email should be found")
	emails = GetEmailsFromString("send email from john@example.com to lisa@example.com")
	assert.Equal(t, 2, len(emails), "should detect 2 emails on string")
	assert.Equal(t, "john@example.com", emails[0], "first email detected should be john@example.com")
	assert.Equal(t, "lisa@example.com", emails[1], "first email detected should be lisa@example.com")
}
