package controller

import (
	"IMB/dto/request"
	"IMB/services"

	"github.com/gin-gonic/gin"
)

func AddFriend(c *gin.Context) {
	var req request.AddFriendRequest
	c.BindJSON(&req)
	res := services.AddFriend(req)
	c.JSON(res.Header, res.Body)
}

func GetFriendList(c *gin.Context) {
	var req request.GetFriendList
	c.BindJSON(&req)
	res := services.GetFriendList(req)
	c.JSON(res.Header, res.Body)
}

func GetCommonFriend(c *gin.Context) {
	var req request.CommonFriendRequest
	c.BindJSON(&req)
	res := services.GetCommonFriend(req)
	c.JSON(res.Header, res.Body)
}
