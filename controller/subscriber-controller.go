package controller

import (
	"IMB/dto/request"
	"IMB/services"

	"github.com/gin-gonic/gin"
)

func Subscribe(c *gin.Context) {
	var req request.SubscribeRequest
	c.BindJSON(&req)
	res := services.Subscribe(req)
	c.JSON(res.Header, res.Body)
}

func Block(c *gin.Context) {
	var req request.BlockRequest
	c.BindJSON(&req)
	res := services.BlockUpdates(req)
	c.JSON(res.Header, res.Body)
}

func Updates(c *gin.Context) {
	var req request.GetUpdatesRequest
	c.BindJSON(&req)
	res := services.GetUpdates(req)
	c.JSON(res.Header, res.Body)
}
