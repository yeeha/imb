package main

import (
	"IMB/controller"
	"IMB/engine"
	_ "IMB/mockup"
	"os"
)

func init() {
	engine.GetEngine().POST("/friend/add", controller.AddFriend)
	engine.GetEngine().POST("/friend/list", controller.GetFriendList)
	engine.GetEngine().POST("/friend/common", controller.GetCommonFriend)
	engine.GetEngine().POST("/subscriber/subscribe", controller.Subscribe)
	engine.GetEngine().POST("/subscriber/block", controller.Block)
	engine.GetEngine().POST("/subscriber/updates", controller.Updates)
}

func main() {
	if len(os.Args) < 3 {
		engine.GetEngine().Run("localhost:1313")
	}
	engine.GetEngine().Run(os.Args[1] + ":" + os.Args[2])
}
