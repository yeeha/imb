package dao

import (
	"IMB/engine"
	"IMB/model"
	"fmt"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

var user1 model.User
var user2 model.User
var user3 model.User
var user4 model.User
var user5 model.User

func TestMain(m *testing.M) {
	user1.Email = "yody@example.com"
	user2.Email = "hariadi@example.com"
	user3.Email = "kurniawan@example.com"
	user4.Email = "subscriber1@example.com"
	user5.Email = "subscriber2@example.com"
	engine.GetORM().Create(&user1)
	engine.GetORM().Create(&user2)
	engine.GetORM().Create(&user3)
	engine.GetORM().Create(&user4)
	engine.GetORM().Create(&user5)
	LinkUsersAsFriend(user1.ID, user2.ID)
	LinkUsersAsFriend(user1.ID, user3.ID)
	Subscribe(user4.ID, user1.ID)
	Subscribe(user5.ID, user1.ID)
	res := m.Run()
	engine.GetORM().Close()
	fmt.Println(os.Remove("imb-test-development.db"))
	os.Exit(res)
}

func TestGetFriendsEmail(t *testing.T) {
	err, emails := GetFriendsEmail("yody@example.com")
	assert.Equal(t, nil, err, "no error expected")
	assert.Equal(t, 2, len(emails), "should return 2 friend")

}

func TestGetCommonFriend(t *testing.T) {
	err, emails := GetCommonFriend(user2.ID, user3.ID)
	assert.Equal(t, nil, err, "no error expected")
	assert.Equal(t, 1, len(emails), "should return 1 friend")
	assert.Equal(t, "yody@example.com", emails[0], "friend email should be yody@example.com")
}

func TestGetUpdates(t *testing.T) {
	err, emails := GetUpdates(user1.Email)
	assert.Equal(t, nil, err, "no error expected")
	assert.Equal(t, 4, len(emails), "should return 4 friend")
}

func TestLinkUsersAsFriend(t *testing.T) {
	assert.Equal(t, nil, LinkUsersAsFriend(user1.ID, user2.ID), "add friend success")
	assert.Equal(t, nil, LinkUsersAsFriend(user4.ID, user1.ID), "add friend success")
	assert.Equal(t, nil, LinkUsersAsFriend(user4.ID, user1.ID), "add friend success")
}

func TestSubscribe(t *testing.T) {
	assert.Equal(t, nil, Subscribe(user5.ID, user1.ID), "success subscribe")
	assert.NotEqual(t, nil, Subscribe(user1.ID, user2.ID), "add friend failed")
}
