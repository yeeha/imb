package dao

import (
	"IMB/engine"
	"IMB/model"
)

func LinkUsersAsFriend(user1 uint, user2 uint) error {
	tx := engine.GetORM().Begin()
	var relation1, relation2 model.Relation
	relation1.UserID = user1
	relation1.RelatedToID = user2
	if !tx.Where(&relation1).First(&relation1).RecordNotFound() {
		relation1.RelationType = model.FRIEND
		relation1.Blocked = false
		if err := tx.Save(&relation1).Error; err != nil {
			tx.Rollback()
			return err
		}
	} else if err := tx.Create(&model.Relation{UserID: user1, RelatedToID: user2, RelationType: model.FRIEND}).Error; err != nil {
		tx.Rollback()
		return err
	}
	relation2.UserID = user2
	relation2.RelatedToID = user1
	if !tx.Where(&relation2).First(&relation2).RecordNotFound() {
		relation2.RelationType = model.FRIEND
		relation2.Blocked = false
		if err := tx.Save(&relation2).Error; err != nil {
			tx.Rollback()
			return err
		}
	} else if err := tx.Create(&model.Relation{UserID: user2, RelatedToID: user1, RelationType: model.FRIEND}).Error; err != nil {
		tx.Rollback()
		return err
	}
	tx.Commit()
	return nil
}

func Subscribe(requestor uint, target uint) error {
	var relation model.Relation
	relation.UserID = requestor
	relation.RelatedToID = target
	relation.RelationType = model.SUBSCRIBER
	if !engine.GetORM().Where(&relation).First(&relation).RecordNotFound() {
		relation.Blocked = false
		if err := engine.GetORM().Save(&relation).Error; err != nil {
			return err
		}
	} else if err := engine.GetORM().Create(&model.Relation{UserID: requestor, RelatedToID: target, RelationType: model.SUBSCRIBER}).Error; err != nil {
		return err
	}
	return nil
}
