package dao

import (
	"IMB/engine"
	"IMB/model"
	"fmt"
)

func getEmailsQuery(sql string, values ...interface{}) (error, []string) {
	var emails []string
	rows, err := engine.GetORM().Raw(sql, values...).Rows()
	defer rows.Close()
	if err != nil {
		fmt.Println(err)
		return err, nil
	}

	for rows.Next() {
		var temp string
		rows.Scan(&temp)
		emails = append(emails, temp)
	}
	fmt.Println(emails)
	return nil, emails
}

func GetFriendsEmail(email string) (error, []string) {
	return getEmailsQuery("select users.email from users,(select * from users join relations on users.id = relations.user_id where relation_type=? and email = ?) as sub where relation_type=? and sub.related_to_id = users.id;", model.FRIEND, email, model.FRIEND)
}

func GetCommonFriend(user1 uint, user2 uint) (error, []string) {
	return getEmailsQuery("select DISTINCT users.email from users join relations on users.id = relations.user_id where relation_type=? and related_to_id in (?,?);", model.FRIEND, user1, user2)
}

func GetUpdates(sender string) (error, []string) {
	return getEmailsQuery("SELECT users.email FROM users,(SELECT relations.user_id FROM users JOIN relations ON users.id = relations.related_to_id WHERE email = ? AND NOT blocked )sub where sub.user_id = users.id", sender)
}
