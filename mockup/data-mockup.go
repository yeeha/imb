package mockup

import (
	"IMB/engine"
	"IMB/model"
)

func init() {
	engine.GetORM().Where(model.User{Email: "john@example.com"}).FirstOrCreate(&model.User{Email: "john@example.com"})
	engine.GetORM().Where(model.User{Email: "andy@example.com"}).FirstOrCreate(&model.User{Email: "andy@example.com"})
	engine.GetORM().Where(model.User{Email: "common@example.com"}).FirstOrCreate(&model.User{Email: "common@example.com"})
	engine.GetORM().Where(model.User{Email: "lisa@example.com"}).FirstOrCreate(&model.User{Email: "lisa@example.com"})
}
