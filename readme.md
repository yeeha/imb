# Social Media API
## Notes From Developer
- When A block B but re-add as friend, the blocked status would be false and A and B would both receive updates from both sides
- When A block B but A re-subscribe as B, the blocked status would be false and A will receive updates from B

# API Spec with Postman
- https://documenter.getpostman.com/view/1686654/imb/6tf1N4N
- import postman .v2 file: postman.json

# Test Cases
- Add Friend
- Get Friend List
- Get Common Friend List
- Subscribe
- Block
- Updates
- Remove From Updates Recipient List After Block
- Re-Add as Friend After Block
- Re-Subscribe After Block
- Appear in Updates List After Block and Re-Add/Re-Subscribe


>**MicroServices** Age it's here. The apps build based on MicroServices concept.
All function splits into several services (add,subscribe,block,get friend and send updates) all micro. It's tidy, easy to maintenance and enhance. Core function are saved into one package and possible to moved as standalone library.
Its Also used **sqlite database**

# Social Media API (Backend)

It's here now, social media application that enable to maintain friendships. Let's add your friend and mantain your circle using this API.
It's GO based, so it's build in concurrency that reduce processing time.

CHEERS....

## Getting Started

GOLANG NEEDED, PLEASE ACCESSS [HERE](https://golang.org/doc/install?download=go1.5.windows-amd64.msi2) TO INSTALL GOLANG:

then to get this project, just simply called:
```
GO GET bitbucket.org/yoedi_har_id/imb/
```
or
```
Git Clone https://yoedi_har_id@bitbucket.org/yoedi_har_id/imb.git
```

### Prerequisites

GOLANG:
```
(https://golang.org/doc/install?download=go1.5.windows-amd64.msi2)
```

API Framework (Gin-Gonic):
```
(https://github.com/gin-gonic/gin)
```

ORM (GORM):
```
(https://github.com/jinzhu/gorm)
```

Sqlite3:
```
(https://github.com/mattn/go-sqlite3)
```

Test Tools (Assert):
```
(https://github.com/stretchr/testify/assert)
```

**ATTENTION**: user used to run the program need to be able to create files (sqlite .db file)

# Build

Linux:
```
./build.sh
```

Windows:
```
./build.bat
```

## Running the tests

Unit test include on build.sh

### Break down into end to end tests

##### Services Test Case:
* Test input output handler
* End to End Test (use given example as test data)

##### Dao,Common,Error Test Case:
* Test functionality (unit test)

### And coding style tests

* It's TDD, test case first then code follow up :)
* Function to return error (nil on OK state)

```go
func TestIsEven(t *testing.T){
	//use lib (Read Prerequisites)
	assert.NotEqual(t, nil, isEven(3),"Test given odd number")
	assert.Equal(t, nil, isEven(2),"Test given even number")
}

func isEven(input int) error {
	//Close Guard
	if input%2 != 0 {
		return errors.New("number is not Even")
	}
	return nil
}
```

## Deployment

Windows:
```
IMB.exe
```

Linux:
```
./IMB
```

## Contributing

Please read [CONTRIBUTING.md](https://gist.github.com/PurpleBooth/b24679402957c63ec426) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

My first day
V 1.0.0

## Authors

* **Yoedi Hariadi Kurniawan** - *Passionate Software Engineer*

## License

## Acknowledgments
