package model

import (
	"IMB/engine"

	"github.com/jinzhu/gorm"
)

type User struct {
	gorm.Model
	Email string `gorm:"unique"`
}

func init() {
	engine.GetORM().AutoMigrate(&User{})
}
