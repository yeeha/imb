package model

import (
	"IMB/engine"

	"github.com/jinzhu/gorm"
)

const (
	FRIEND     = "FRIEND"
	SUBSCRIBER = "SUBSCRIBER"
)

type Relation struct {
	gorm.Model
	UserID       uint `gorm:"unique_index:idx_relations"`
	RelatedToID  uint `gorm:"unique_index:idx_relations"`
	RelationType string
	Blocked      bool `gorm:"default:false"`
}

func init() {
	engine.GetORM().AutoMigrate(&Relation{})
}
