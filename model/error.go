package model

import (
	"IMB/engine"

	"github.com/jinzhu/gorm"
)

type Error struct {
	gorm.Model
	StackTrace string
}

func init() {
	engine.GetORM().AutoMigrate(&Error{})
}
