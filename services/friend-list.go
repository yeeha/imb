package services

import (
	"IMB/dao"
	"IMB/dto/request"
	"IMB/dto/response"
	"IMB/engine"
	"IMB/error"
	"IMB/model"
)

func GetFriendList(req request.GetFriendList) response.Response {
	var res response.Response
	if err := engine.GetORM().Where(&model.User{Email: req.Email}).First(&model.User{}).Error; err != nil {
		return error.Handler(req.Email+" not registered", err)
	}
	err, emails := dao.GetFriendsEmail(req.Email)
	if err != nil {
		return error.Handler("error getting friend list", err)
	}
	if len(emails) == 0 {
		res.Header = 200
		res.Body = &response.ValidatedResponse{Success: true, Message: "no friends on your list"}
		return res
	}
	res.Header = 200
	res.Body = &response.FriendListResponse{Success: true, Friends: emails, Count: len(emails)}
	return res
}
