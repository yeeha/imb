package services

import (
	"IMB/dto/request"
	"IMB/dto/response"
	"IMB/engine"
	"IMB/error"
	"IMB/model"
	"fmt"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

var user1 model.User
var user2 model.User
var user3 model.User
var user4 model.User
var user5 model.User
var user6 model.User
var user7 model.User
var user8 model.User

func TestMain(m *testing.M) {
	user1.Email = "yody@example.com"
	user2.Email = "hariadi@example.com"
	user3.Email = "kurniawan@example.com"
	user4.Email = "subsriber1@example.com"
	user5.Email = "subscriber2@example.com"
	user6.Email = "commoner1@example.com"
	user7.Email = "commoner2@example.com"
	user8.Email = "block@example.com"
	engine.GetORM().Create(&user1)
	engine.GetORM().Create(&user2)
	engine.GetORM().Create(&user3)
	engine.GetORM().Create(&user4)
	engine.GetORM().Create(&user5)
	engine.GetORM().Create(&user6)
	engine.GetORM().Create(&user7)
	engine.GetORM().Create(&user8)
	res := m.Run()
	engine.GetORM().Close()
	fmt.Println(os.Remove("imb-test-development.db"))
	os.Exit(res)
}

func TestAddFriend(t *testing.T) {
	var req request.AddFriendRequest
	var res response.Response
	var errorRes error.Error
	var addFriendRes response.AddFriendResponse
	var err, err2 model.Error

	req.Friends[0] = "notregistered@example.com"
	req.Friends[1] = "yody@example.com"
	res = AddFriend(req)
	engine.GetORM().Last(&err)
	errorRes = error.Error{Success: false, Message: "notregistered@example.com not registered", Stacktrace: err.ID}
	assert.Equal(t, 500, res.Header, "test user1 email not registered header")
	assert.Equal(t, &errorRes, res.Body, "test user1 email not registered body")

	req.Friends[0] = "yody@example.com"
	req.Friends[1] = "notregistered@example.com"
	res = AddFriend(req)
	engine.GetORM().Last(&err2)
	errorRes = error.Error{Success: false, Message: "notregistered@example.com not registered", Stacktrace: err2.ID}
	assert.Equal(t, 500, res.Header, "test user2 email not registered header")
	assert.Equal(t, &errorRes, res.Body, "test user2 email not registered body")

	req.Friends[0] = "yody@example.com"
	req.Friends[1] = "hariadi@example.com"
	res = AddFriend(req)
	addFriendRes = response.AddFriendResponse{Success: true, Message: "Users are friends now"}
	assert.Equal(t, 201, res.Header, "test success header")
	assert.Equal(t, &addFriendRes, res.Body, "test success body")
}
func TestGetFriendList(t *testing.T) {
	var req request.GetFriendList
	var res response.Response
	var errorRes error.Error
	var getFriendListRes response.FriendListResponse
	var err model.Error
	var validatedRes response.ValidatedResponse

	req.Email = "notRegistered@example.com"
	res = GetFriendList(req)
	engine.GetORM().Last(&err)
	errorRes = error.Error{Success: false, Message: "notRegistered@example.com not registered", Stacktrace: err.ID}
	assert.Equal(t, 500, res.Header, "test user email not registered header")
	assert.Equal(t, &errorRes, res.Body, "test user email not registered body")

	req.Email = "kurniawan@example.com"
	res = GetFriendList(req)
	validatedRes = response.ValidatedResponse{Success: true, Message: "no friends on your list"}
	assert.Equal(t, 200, res.Header, "test user email not registered header")
	assert.Equal(t, &validatedRes, res.Body, "test user email not registered body")

	var reqAddFriend request.AddFriendRequest
	reqAddFriend.Friends[0] = "hariadi@example.com"
	reqAddFriend.Friends[1] = "kurniawan@example.com"
	AddFriend(reqAddFriend)

	req.Email = "kurniawan@example.com"
	res = GetFriendList(req)
	var emails []string
	emails = append(emails, "hariadi@example.com")
	getFriendListRes = response.FriendListResponse{Success: true, Friends: emails, Count: 1}
	assert.Equal(t, 200, res.Header, "test user email not registered header")
	assert.Equal(t, &getFriendListRes, res.Body, "test user email not registered body")
}
func TestGetCommonFriend(t *testing.T) {
	var req request.CommonFriendRequest
	var res response.Response
	var errorRes error.Error
	var commonFriendRes response.CommonFriendResponse
	var validatedRes response.ValidatedResponse
	var err, err2 model.Error

	req.Friends[0] = "notregistered@example.com"
	req.Friends[1] = "yody@example.com"
	res = GetCommonFriend(req)
	engine.GetORM().Last(&err)
	errorRes = error.Error{Success: false, Message: "notregistered@example.com not registered", Stacktrace: err.ID}
	assert.Equal(t, 500, res.Header, "test user1 email not registered header")
	assert.Equal(t, &errorRes, res.Body, "test user1 email not registered body")

	req.Friends[0] = "yody@example.com"
	req.Friends[1] = "notregistered@example.com"
	res = GetCommonFriend(req)
	engine.GetORM().Last(&err2)
	errorRes = error.Error{Success: false, Message: "notregistered@example.com not registered", Stacktrace: err2.ID}
	assert.Equal(t, 500, res.Header, "test user2 email not registered header")
	assert.Equal(t, &errorRes, res.Body, "test user2 email not registered body")

	req.Friends[0] = user6.Email
	req.Friends[1] = user7.Email
	res = GetCommonFriend(req)
	validatedRes = response.ValidatedResponse{Success: true, Message: "no common friend found"}
	assert.Equal(t, 200, res.Header, "test no common friend header")
	assert.Equal(t, &validatedRes, res.Body, "test no common friend body")

	var reqAddFriend request.AddFriendRequest
	reqAddFriend.Friends[0] = user6.Email
	reqAddFriend.Friends[1] = "yody@example.com"
	AddFriend(reqAddFriend)
	reqAddFriend.Friends[0] = user7.Email
	reqAddFriend.Friends[1] = "yody@example.com"
	AddFriend(reqAddFriend)

	req.Friends[0] = user6.Email
	req.Friends[1] = user7.Email
	res = GetCommonFriend(req)
	var emails []string
	emails = append(emails, "yody@example.com")
	commonFriendRes = response.CommonFriendResponse{Success: true, Friends: emails, Count: len(emails)}
	assert.Equal(t, 200, res.Header, "test success header")
	assert.Equal(t, &commonFriendRes, res.Body, "test success body")

}
func TestSubscribe(t *testing.T) {
	var req request.SubscribeRequest
	var res response.Response
	var errorRes error.Error
	var subscribeRes response.SubscribeResponse
	var err, err2 model.Error

	req.Requestor = "notregistered@example.com"
	req.Target = "yody@example.com"
	res = Subscribe(req)
	engine.GetORM().Last(&err)
	errorRes = error.Error{Success: false, Message: "notregistered@example.com not registered", Stacktrace: err.ID}
	assert.Equal(t, 500, res.Header, "test user1 email not registered header")
	assert.Equal(t, &errorRes, res.Body, "test user1 email not registered body")

	req.Requestor = "yody@example.com"
	req.Target = "notregistered@example.com"
	res = Subscribe(req)
	engine.GetORM().Last(&err2)
	errorRes = error.Error{Success: false, Message: "notregistered@example.com not registered", Stacktrace: err2.ID}
	assert.Equal(t, 500, res.Header, "test user2 email not registered header")
	assert.Equal(t, &errorRes, res.Body, "test user2 email not registered body")

	req.Requestor = "subsriber1@example.com"
	req.Target = "yody@example.com"
	res = Subscribe(req)
	subscribeRes = response.SubscribeResponse{Success: true, Message: "you now subscribe " + user1.Email}
	assert.Equal(t, 201, res.Header, "test success header")
	assert.Equal(t, &subscribeRes, res.Body, "test success body")
}
func TestBlockUpdates(t *testing.T) {
	var req request.BlockRequest
	var res response.Response
	var errorRes error.Error
	var blockRes response.BlockResponse
	var vaidatedRes response.ValidatedResponse
	var err, err2, err3 model.Error

	req.Requestor = "notregistered@example.com"
	req.Target = "yody@example.com"
	res = BlockUpdates(req)
	engine.GetORM().Last(&err)
	errorRes = error.Error{Success: false, Message: "notregistered@example.com not registered", Stacktrace: err.ID}
	assert.Equal(t, 500, res.Header, "test user1 email not registered header")
	assert.Equal(t, &errorRes, res.Body, "test user1 email not registered body")

	req.Requestor = "yody@example.com"
	req.Target = "notregistered@example.com"
	res = BlockUpdates(req)
	engine.GetORM().Last(&err2)
	errorRes = error.Error{Success: false, Message: "notregistered@example.com not registered", Stacktrace: err2.ID}
	assert.Equal(t, 500, res.Header, "test user2 email not registered header")
	assert.Equal(t, &errorRes, res.Body, "test user2 email not registered body")

	req.Requestor = "block@example.com"
	req.Target = "yody@example.com"
	res = BlockUpdates(req)
	engine.GetORM().Last(&err3)
	errorRes = error.Error{Success: false, Message: "relation between users not found", Stacktrace: err3.ID}
	assert.Equal(t, 500, res.Header, "test block non related users header")
	assert.Equal(t, &errorRes, res.Body, "test block non related users body")

	var reqSubscribe request.SubscribeRequest
	reqSubscribe.Requestor = user8.Email
	reqSubscribe.Target = user1.Email
	fmt.Println(Subscribe(reqSubscribe))

	req.Requestor = "block@example.com"
	req.Target = "yody@example.com"
	res = BlockUpdates(req)
	blockRes = response.BlockResponse{Success: true, Message: "you now block updates from " + user1.Email}
	assert.Equal(t, 201, res.Header, "test success block header")
	assert.Equal(t, &blockRes, res.Body, "test success block body")

	req.Requestor = "block@example.com"
	req.Target = "yody@example.com"
	res = BlockUpdates(req)
	vaidatedRes = response.ValidatedResponse{Success: true, Message: "you already block updates from :" + user1.Email}
	assert.Equal(t, 200, res.Header, "test duplicate block request header")
	assert.Equal(t, &vaidatedRes, res.Body, "test duplicate block request body")
}
func TestGetUpdates(t *testing.T) {
	var req request.GetUpdatesRequest
	var res response.Response
	var errorRes error.Error
	var getUpdatesRes response.GetUpdatesResponse
	var err model.Error

	req.Sender = "notregistered@example.com"
	req.Text = "start mentioned@example.com end"
	res = GetUpdates(req)
	engine.GetORM().Last(&err)
	errorRes = error.Error{Success: false, Message: "notregistered@example.com sender email not found", Stacktrace: err.ID}
	assert.Equal(t, 500, res.Header, "test user1 email not registered header")
	assert.Equal(t, &errorRes, res.Body, "test user1 email not registered body")

	var reqSubscribe request.SubscribeRequest
	reqSubscribe.Requestor = user8.Email
	reqSubscribe.Target = user5.Email
	fmt.Println(Subscribe(reqSubscribe))

	req.Sender = user5.Email
	req.Text = "start mentioned@example.com end"
	res = GetUpdates(req)
	engine.GetORM().Last(&err)
	var emails []string
	emails = append(emails, "mentioned@example.com")
	emails = append(emails, user8.Email)
	getUpdatesRes = response.GetUpdatesResponse{Success: true, Recipients: emails}
	assert.Equal(t, 200, res.Header, "test success header")
	assert.Equal(t, &getUpdatesRes, res.Body, "test getUpdatesRes body")
}
