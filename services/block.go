package services

import (
	"IMB/dto/request"
	"IMB/dto/response"
	"IMB/engine"
	"IMB/error"
	"IMB/model"
)

func BlockUpdates(req request.BlockRequest) response.Response {
	var res response.Response
	var user1 model.User
	var user2 model.User

	user1.Email = req.Requestor
	user2.Email = req.Target

	println(user1.Email)
	println(user2.Email)

	if err := engine.GetORM().Where(&user1).First(&user1).Error; err != nil {
		return error.Handler(user1.Email+" not registered", err)
	}
	if err := engine.GetORM().Where(&user2).First(&user2).Error; err != nil {
		return error.Handler(user2.Email+" not registered", err)
	}
	var relation model.Relation
	relation.UserID = user1.ID
	relation.RelatedToID = user2.ID
	if err := engine.GetORM().Where(&relation).First(&relation).Error; err != nil {
		return error.Handler("relation between users not found", err)
	}
	if relation.Blocked {
		res.Header = 200
		res.Body = &response.ValidatedResponse{Success: true, Message: "you already block updates from :" + user2.Email}
		return res
	}
	relation.Blocked = true
	if err := engine.GetORM().Save(&relation).Error; err != nil {
		return error.Handler("failed to block updates", err)
	}

	res.Header = 201
	res.Body = &response.BlockResponse{Success: true, Message: "you now block updates from " + user2.Email}
	return res
}
