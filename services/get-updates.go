package services

import (
	"IMB/common"
	"IMB/dao"
	"IMB/dto/request"
	"IMB/dto/response"
	"IMB/engine"
	"IMB/error"
	"IMB/model"
)

func GetUpdates(req request.GetUpdatesRequest) response.Response {
	var res response.Response
	emails := common.GetEmailsFromString(req.Text)
	var sender model.User
	sender.Email = req.Sender
	if err := engine.GetORM().Where(&sender).First(&sender).Error; err != nil {
		return error.Handler(req.Sender+" sender email not found", err)
	}
	err, tempEmails := dao.GetUpdates(sender.Email)
	if err != nil {
		return error.Handler("cannot get updates email list", err)
	}
	emails = append(emails, tempEmails...)
	res.Header = 200
	res.Body = &response.GetUpdatesResponse{Success: true, Recipients: emails}
	return res
}
