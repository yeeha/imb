package services

import (
	"IMB/dao"
	"IMB/dto/request"
	"IMB/dto/response"
	"IMB/engine"
	"IMB/error"
	"IMB/model"
)

func GetCommonFriend(req request.CommonFriendRequest) response.Response {
	var res response.Response
	var user1 model.User
	var user2 model.User
	user1.Email = req.Friends[0]
	user2.Email = req.Friends[1]
	if err := engine.GetORM().Where(&user1).First(&user1).Error; err != nil {
		return error.Handler(user1.Email+" not registered", err)
	}
	if err := engine.GetORM().Where(&user2).First(&user2).Error; err != nil {
		return error.Handler(user2.Email+" not registered", err)
	}
	err, emails := dao.GetCommonFriend(user1.ID, user2.ID)
	if err != nil {
		return error.Handler("error get common friend", err)
	}
	if len(emails) == 0 {
		res.Header = 200
		res.Body = &response.ValidatedResponse{Success: true, Message: "no common friend found"}
		return res
	}
	res.Header = 200
	res.Body = &response.CommonFriendResponse{Success: true, Friends: emails, Count: len(emails)}
	return res
}
