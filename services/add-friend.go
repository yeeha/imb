package services

import (
	"IMB/dao"
	"IMB/dto/request"
	"IMB/dto/response"
	"IMB/engine"
	"IMB/error"
	"IMB/model"
)

func AddFriend(req request.AddFriendRequest) response.Response {
	var res response.Response
	var user1 model.User
	var user2 model.User

	user1.Email = req.Friends[0]
	user2.Email = req.Friends[1]

	println(user1.Email)
	println(user2.Email)

	if err := engine.GetORM().Where(&user1).First(&user1).Error; err != nil {
		return error.Handler(user1.Email+" not registered", err)
	}
	if err := engine.GetORM().Where(&user2).First(&user2).Error; err != nil {
		return error.Handler(user2.Email+" not registered", err)
	}

	if err := dao.LinkUsersAsFriend(user1.ID, user2.ID); err != nil {
		return error.Handler("failed create relations, check your friends list", err)
	}

	res.Header = 201
	res.Body = &response.AddFriendResponse{Success: true, Message: "Users are friends now"}
	return res
}
