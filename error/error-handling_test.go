package error

import (
	"IMB/engine"
	"IMB/model"
	"errors"
	"fmt"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestHandler(t *testing.T) {
	res := Handler("testong", errors.New("test error"))
	assert.Equal(t, 500, res.Header, "http code 500")
	var err model.Error
	engine.GetORM().First(&err)
	var response Error
	response.Message = "testong"
	response.Stacktrace = err.ID
	response.Success = false
	assert.Equal(t, &response, res.Body, "error body")

	engine.GetORM().Close()
	fmt.Println(os.Remove("imb-test-development.db"))
}
