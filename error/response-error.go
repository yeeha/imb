package error

type Error struct {
	Success    bool
	Message    string
	Stacktrace uint
}
