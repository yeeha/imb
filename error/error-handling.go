package error

import (
	"IMB/dto/response"
	"IMB/engine"
	"IMB/model"
)

func Handler(message string, err error) response.Response {
	var error model.Error
	error.StackTrace = err.Error()
	engine.GetORM().Create(&error)
	return response.Response{Header: 500, Body: &Error{Success: false, Message: message, Stacktrace: error.ID}}
}
