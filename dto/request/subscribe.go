package request

type SubscribeRequest struct {
	Requestor string
	Target    string
}
