package request

type BlockRequest struct {
	Requestor string
	Target    string
}
