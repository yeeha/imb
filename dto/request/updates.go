package request

type GetUpdatesRequest struct {
	Sender string
	Text   string
}
