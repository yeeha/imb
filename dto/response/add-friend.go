package response

type AddFriendResponse struct {
	Success bool
	Message string
}
