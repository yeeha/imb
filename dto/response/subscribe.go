package response

type SubscribeResponse struct {
	Success bool
	Message string
}
