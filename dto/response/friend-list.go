package response

type FriendListResponse struct {
	Success bool
	Friends []string
	Count   int
}
