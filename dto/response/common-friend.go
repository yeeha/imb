package response

type CommonFriendResponse struct {
	Success bool
	Friends []string
	Count   int
}
