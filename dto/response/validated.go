package response

type ValidatedResponse struct {
	Success bool
	Message string
}
