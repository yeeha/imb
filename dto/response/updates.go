package response

type GetUpdatesResponse struct {
	Success    bool
	Recipients []string
}
