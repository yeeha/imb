package response

type BlockResponse struct {
	Success bool
	Message string
}
